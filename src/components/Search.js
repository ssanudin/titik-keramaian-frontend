import React, { useState } from "react";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import MobileDateTimePicker from "@mui/lab/MobileDateTimePicker";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";

const Search = ({
  timeFrom,
  timeTo,
  handleTimeFrom,
  handleTimeTo,
  handleSearch,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClickMenuSearch = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleCloseMenuSearch = () => {
    setAnchorEl(null);
  };
  return (
    <div className="menu-search">
      <Button
        id="search-button"
        variant="contained"
        aria-controls={open ? "basic-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClickMenuSearch}
      >
        Search Data
      </Button>
      <Menu
        id="menu-search-btn"
        anchorEl={anchorEl}
        open={open}
        onClose={handleCloseMenuSearch}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
        sx={{ marginTop: ".5rem" }}
      >
        <MenuItem>
          <Stack
            component="form"
            direction={{ xs: "column", sm: "row" }}
            spacing={{ xs: 1, sm: 2, md: 4 }}
          >
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <MobileDateTimePicker
                label="From"
                value={timeFrom}
                onChange={handleTimeFrom}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <MobileDateTimePicker
                label="To"
                value={timeTo}
                onChange={handleTimeTo}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
            <Button variant="contained" onClick={handleSearch}>
              Search
            </Button>
          </Stack>
        </MenuItem>
      </Menu>
    </div>
  );
};

export default Search;
